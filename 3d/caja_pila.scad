$fn=700;

$LARGO = 55;
$ALTO = 35;
$ANCHO = 25;

// GROSOR
$G = 5;


difference() {
cube([$LARGO+$G, $ANCHO+$G, $ALTO+$G]);

translate([$G/2, $G/2, $G/2]) cube([$LARGO+$G, $ANCHO, $ALTO]);
    
rotate([90,0,0]) translate([2,12.5,-5]) linear_extrude(height = 5) {
        text("CAR", size=19, $fn = 500);
    }

}
/* 
Medidas de la pila:
Largo 5 (ondo)
alto 3 (de arriba a bajo)
ancho 2 (derecha-izquierda)
*/