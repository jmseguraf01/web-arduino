$fn = 64;
$ancho = 98;
$largo = 160;
$alto = 100;
$grosor = 10;

// El cubo prinicpal

//translate([0,-$grosor/3-10, -$grosor/2]) cube([$ancho-$grosor,$largo, $alto-$grosor+60],center=true);

difference()  {
   


   hull() {
cube( [$ancho,$largo, $alto],center=true);
rotate([0,90]) translate([-70,-75,-$ancho/2]) cylinder(r=10,h=$ancho);
rotate([0,90]) translate([-70,75,-$ancho/2]) cylinder(r=10,h=$ancho);

      
}
   

// Este es el cubo que se le resta al otro
translate([0,-$grosor/3-5, -$grosor/2]) cube([$ancho-$grosor,$largo+10, $alto-$grosor+60],center=true);
  
// Rectangulo de la derecha
translate([-$ancho,-$largo/3,-$alto/2]) cube([$ancho,$largo/2+2,$alto/6]);

// Rectangulo de la izquierda
translate([$ancho/4,-$largo/3,-$alto/2]) cube([$ancho,$largo/2+2,$alto/6]);


/*
---- Para los tornillos ----
  Se hace un agujero cada 5mm.
*/

// Lado derecho
 for (x =[-45:6:105])
        translate([-$ancho/2.1,(x)-30.4,-$alto/1.99]) cylinder(r=1,h=10); 
 
// Lado izquierdo
 for (x =[-45:6:105])
        translate([$ancho/2.1,(x)-30.4,-$alto/1.99]) cylinder(r=1,h=10); 

 
// La primera ventana izquierda
rotate([0,90,0]) translate([-20,-35,36]) cylinder(r=15,h=15);

// La segunda ventana izquierda
rotate([0,90,0]) translate([-20,35,36]) cylinder(r=15,h=15);

// Primera ventana de la derecha
rotate([0,90,0]) translate([-20,-35,-50]) cylinder(r=15,h=15);
 

// Segunda ventana de la derecha
rotate([0,90,0]) translate([-20,35,-50]) cylinder(r=15,h=15);

// Redondas random derecha
rotate([0,90,0]) translate([-50,65,-50]) cylinder(r=10,h=15);
rotate([0,90,0]) translate([35,65,-50]) cylinder(r=10,h=15);
rotate([0,90,0]) translate([20,35,-50]) cylinder(r=10,h=15);
rotate([0,90,0]) translate([22,-33,-50]) cylinder(r=8,h=15);
rotate([0,90,0]) translate([-67,-35,-50]) cylinder(r=8,h=15);
rotate([0,90,0]) translate([-57,05,-50]) cylinder(r=12,h=15);
rotate([0,90,0]) translate([10,-59,-50]) cylinder(r=7,h=15);
rotate([0,90,0]) translate([10,59,-50]) cylinder(r=7,h=15);

// Redondas random izquierda
rotate([0,90,0]) translate([-50,65,39]) cylinder(r=10,h=15);
rotate([0,90,0]) translate([35,65,36]) cylinder(r=10,h=15);
rotate([0,90,0]) translate([20,35,36]) cylinder(r=10,h=15);
rotate([0,90,0]) translate([22,-33,36]) cylinder(r=8,h=15);
rotate([0,90,0]) translate([-67,-35,36]) cylinder(r=8,h=15);
rotate([0,90,0]) translate([-57,05,36]) cylinder(r=12,h=15);
rotate([0,90,0]) translate([10,-59,36]) cylinder(r=7,h=15);
rotate([0,90,0]) translate([10,59,36]) cylinder(r=7,h=15);

}

// rotate([0,90,0]) translate([-50,65,-46]) cylinder(r=15,h=15);


// Cilindro que hace de nariz en el lado izquierdo
rotate([0,90]) translate([5,-5,45]) cylinder(r=10,h=5);

// Cilindro que hace de nariz en el lado derecho
rotate([0,90]) translate([5,-5,-50]) cylinder(r=10,h=5);

//rotate([0,90]) translate([-70,-75,-60]) cylinder(r=10,h=120);




/*
---------pruebas----------
-----------------------------
//translate([-0,-5, -0]) cube([$ancho-$grosor,$largo-5, $alto+$grosor],center=true);
// Rectangulo de la derecha
translate([-$ancho,-$largo/4,-$alto/2]) cube([$ancho,$largo/2,$alto/6]);

// Rectangulo de la izquierda
translate([$ancho/4,-$largo/4,-$alto/2]) cube([$ancho,$largo/2,$alto/6]);



//translate([-$ancho/2.2,$largo/3,-$alto/1.99]) cylinder(r=2,h=cos(i*10)*50+60);

//translate([-$ancho,($largo/2)-30.4,-$alto/1.99]) cylinder(r=2,h=10);

*/

//translate([-$ancho/2.2,($largo/2)-30.4,-$alto/1.99])
//rotate([90]) translate([50,25,-30]) cylinder(r=15,h=15);


//rotate([90,0,-90]) translate([-25,5,40]) text("Arduino Car");

// translate([$ancho/2.2,($largo/2)-30.4,-$alto/1.99]) cylinder(r=1,h=10);



module roundedcube(xdim ,ydim ,zdim,rdim){
hull(){
translate([rdim,rdim,rdim])sphere(r=rdim);
translate([xdim-rdim,rdim,rdim])sphere(r=rdim);
translate([rdim,ydim-rdim,rdim])sphere(r=rdim);
translate([xdim-rdim,ydim-rdim,rdim])sphere(r=rdim);
translate([rdim,rdim,zdim-rdim])sphere(r=rdim);
translate([xdim-rdim,rdim,zdim-rdim])sphere(r=rdim);
translate([rdim,ydim-rdim,zdim-rdim])sphere(r=rdim);
translate([xdim-rdim,ydim-rdim,zdim-rdim])sphere(r=rdim);   
}
}

