$fn=200;
color("white") translate([0,0,10]) sphere(10); // Cuerpo
color("white") translate([0,0,26]) sphere(7);  // Arriba
color("white") translate([5,2.5,28.5]) rotate([0,90,0]) cylinder(2); //Ojo izquierda
color("black") translate([6.8,2.5,28.4]) rotate([0,90,0]) cylinder(0.3,0.3,0.3); //Iris del ojo izquierda
color("white") translate([5,-2.5,28.5]) rotate([0,90,0]) cylinder(2); // Ojo derecha
color("black") translate([6.8,-2.5,28.4]) rotate([0,90,0]) cylinder(0.3,0.3,0.3); //Iris del ojo izquierda

// Le resto la diferencia de un cuadrado a un cilindro para hacer la boca
difference() {
    color("orange") translate([0,0,24]) rotate([0,90,0]) cylinder(7);
    translate([6,-1,24.3]) cube([2,2,2]);
}

color("white") translate([0,-8,14]) rotate([50,0,0]) cylinder(10); // Brazo izquierda
color("black") translate([0,-16,20.7]) rotate([0,0,0]) sphere(1.5);   // Mano izquierda
color("white") color() translate([0,8,14]) rotate([-50,0,0]) cylinder(10);   // Brazo derecha
color("black") translate([0,16,20.7]) rotate([0,0,0]) sphere(1.5);   // Mano derecha
color("orange") translate([10,0,26.5]) rotate([0,-90,0]) cylinder(3.5,0.2); // Nariz

color("black") translate([0,0.2,32]) circle(6); // Soporte del gorro
color("black") translate([0,0.2,32]) rotate([0,0,0]) cylinder(5,5,5); //Gorro

color("blue") translate([0,0.2,0]) circle(15); // Soporte para el suelo

// Llavero
difference() {
translate([10,-5,0]) rotate([90,0,90]) cube([10,5,2]);
translate([12,-3.5, 1.5]) rotate([90,0,90]) text("TUX", size=2.5, $fn = 500);
}