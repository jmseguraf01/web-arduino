# Arduino Car
- Arduino Car es nuestro proyecto de final de curso, este es un coche con un sensor de ultrasonido y un servo que permite esquivar cualquier objeto que se encuentre en su recorrido.
- Nosotros somos dos estudiantes de __SMX__ (_Sistemas Microinformaticos y Redes_):
   - Juan Miguel Segura Fernandez (_https://gitlab.com/jmseguraf01_ / __jusefer2@hotmail.es__)
   - Alejandro Mallen Gomez (_https://gitlab.com/amalleng01_ / __amalleng01@elpuig.xeill.net__)
- __En resumen__, este es un coche creado por nosotros mismos que se comporta de una forma totalmente autónoma.

## Fotos
* Algunas de las fotos, que puede encontrar en nuestra página web, són:
![La foto no se encuentra disponible](images/foto5.jpg)
![La foto no se encuentra disponible](images/foto4.jpg)

# Página Web
Visite la __pagina web__ de nuestro coche, creada por nosotros con HTML5, CSS y Javascript:
https://jmseguraf01.gitlab.io/web-arduino

![La foto no se encuentra disponible](images/captura_paginaweb.PNG)
