/* Juan Miguel Segura y Alejandro Mallen
Trabajo de sintesi 2n SMX
Licencia GPL
*/

// Incluimos la libreria para controlar el servo y el sensor de ultrasonido
#include <Servo.h>
#include <NewPing.h>


// Aqui se configuran los pines donde debemos conectar el sensor
#define TRIGGER_PIN  2
#define ECHO_PIN     3
#define MAX_DISTANCE 200

// Variables del motor A
int ENA = 6;  
int IN1 = 13;
int IN2 = 12;

// Variables del motor B
int ENB = 5;
int IN3 = 11;
int IN4 = 10;

// Variable del servo
Servo servoMotor;

// Indicamos la variable de la velocidad
int vel = 80;

// Funcion de setup
void setup() {
Serial.begin(9600);
 // Declaramos todos los pines como salidas
 pinMode (ENA, OUTPUT);
 pinMode (ENB, OUTPUT);
 pinMode (IN1, OUTPUT);
 pinMode (IN2, OUTPUT);
 pinMode (IN3, OUTPUT);
 pinMode (IN4, OUTPUT);
 servoMotor.attach(9); // Declaramos el servo para que trabaje con el pin 9
}

// Funcion 'loop' es lo que se ejecuta
void loop() {
  move_servo();
//sensor_ultrasonido(90);
}


void Adelante (int time)
{
 //Direccion motor A
 digitalWrite (IN1, LOW);
 digitalWrite (IN2, HIGH);
 analogWrite (ENA, vel); //Velocidad motor A
 //Direccion motor B
 digitalWrite (IN3, LOW);
 digitalWrite (IN4, HIGH);
 analogWrite (ENB, 87); //Velocidad motor B / Le pongo mas porque hay diferencia y si le pongo un numero mas bajo, el coche va girado / Diferencia de 7 puntos

 delay(time);
}

void Atras (int time)
{
 //Direccion motor A
 digitalWrite (IN1, HIGH);
 digitalWrite (IN2, LOW);
 analogWrite (ENA, vel); //Velocidad motor A
 //Direccion motor B
 digitalWrite (IN3, HIGH);
 digitalWrite (IN4, LOW);
 analogWrite (ENB, vel); //Velocidad motor B

 delay(time);
}


void Derecha (int time)
{
 //Direccion motor A
 digitalWrite (IN1, LOW);
 digitalWrite (IN2, HIGH);
 analogWrite (ENA, vel); //Velocidad motor A
 //Direccion motor B
 digitalWrite (IN3, HIGH);
 digitalWrite (IN4, LOW);
 analogWrite (ENB, vel); //Velocidad motor A

  delay(time);

}

// Funcion para parar el coche
void Parar(int time){
  //Direccion motor A
  digitalWrite (IN1, LOW);
  digitalWrite (IN2, LOW);
  analogWrite (ENA, vel); //Velocidad motor A
  //Direccion motor B
  digitalWrite (IN3, LOW);
  digitalWrite (IN4, LOW);
  analogWrite (ENB, vel); //Velocidad motor B
  delay(time);     
}

void Izquierda (int time)
{
 //Direccion motor A
 digitalWrite (IN1, HIGH);
 digitalWrite (IN2, LOW);
 analogWrite (ENA, vel); //Velocidad motor A
 //Direccion motor B
 digitalWrite (IN3, LOW);
 digitalWrite (IN4, HIGH);
 analogWrite (ENB, vel); //Velocidad motor B

 delay(time);
}

// Funcion para mover el servo
void move_servo() {
// Vamos a tener dos bucles uno para mover en sentido positivo y otro en sentido negativo
/*
180º = esta en la izquierda
90º = en medio
0º = esta en la derecha
 */
 
// Para el sentido positivo
  for (int i = 25; i <= 155; i+=5) { // i = i +5
    // Desplazamos el servo al angulo de la variable i, cada vez se ira incrementando.
    servoMotor.write(i);
    // Hacemos una pausa de 25ms por cada cambio de angulo
    delay(1);
    // Cuando el servo llega a 90º, llama al sensor y escanea
    if ( i == 90 ) {
      sensor_ultrasonido(i);
      }
    }
   // Cuando acacava de hacer el bucle que esta en 180º llama al sensor
   // Le pongo 180 porque el servo esta en el grado 180
   sensor_ultrasonido(155);

  // Para el sentido negativo
  for (int i = 155; i > 25; i-=5) { // i = i - 5
    servoMotor.write(i);
    delay(1);
    // Cuando el servo llega a 90º, llama al sensor y escanea
    if ( i == 90 ) {
      sensor_ultrasonido(i);
      }
  }

  // Cuando acacava de hacer el bucle que esta en 0º llama al sensor
  // Le pongo 0 porque el servo esta en el grado 0
  sensor_ultrasonido(25);
}

void sensor_ultrasonido(int i) {
  NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);
  delay(250);  // Esperar 1 segundo entre mediciones
  int uS = sonar.ping_cm();  // Obtener medicion del objeto en cm
  
  
  // Si hay un objeto en una distancia menor a 15
  if ( uS && uS < 25) {
    
    // Esta en la derecha, hago que pare y que gire a la izquierda
    if (i == 25) {
       Parar(1000);
       Izquierda(500);
       Adelante(0);
      }
  
    // Si hay un obstaculo en medio hago que vaya girando y vaya escaneando hasta que no hay
    else if (i == 90){   
      Atras(500);
      Derecha(500 );
      Serial.print("Objeto detectado en medio");
    
      /*while ( uS < 25 ) {

        // Si el coche esta muy cerca del obstaculo tira para atras
        if (uS < 6 ) {
          Atras(150);
          }

        // Vuelvo a mirar a ver si hay algun obstaculo
        uS = sonar.ping_cm();
        Derecha(150);               
        Parar(1000);
        Serial.print("obstaculo");
        }*/
      }

  // Hay un obstaculo en la izquierda, hago que gire a la derecha
    else if ( i == 155) {
      Atras(500);
      Derecha(500);
      Adelante(0);
      }
  }

  // Si no hay ningun objeto sigue adelante
  else {
   Adelante(0); 
   }

}
