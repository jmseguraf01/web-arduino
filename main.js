// Inicio el script de Jquery
$(document).ready(function(){
  // Para que cuando se recargue la pagina suba al inicio
  $('html, body').animate({scrollTop: ($('#Inicio').offset().top)},500);


  // Cuando se clicka a la div de see_menu se ejecuta la siguiente funcion
  $( "#see_menu" ).click(function() {
    // Si se ve en un movil, ocultamos todos los posits
    if ( $(window).width() <= 1000 ) {
      for (var i = 1; i < 4; i++) {
        $("#posit"+i+"").css("display", "none");
      }
      $(".text_one_page").css("display", "none");
    }
    // Animaciones para el menu
    $("#sections_menu").css("display","block");
    $("#see_menu").css("display", "none"); // Para ocultar el div al que le das y sale el menu
    $("#sections_menu").css("animation","menu_animation_entry 0.5s linear both");  // Le añado una animacion definida en el css
    $("#hide_menu").css("animation","entry_hide_menu 1s linear both");  // Le añado una animacion de entrada al boton de hide menu
    $("#hide_menu").css("display","block");  // Hago que se vea el otro div para ocultar el menu
  });

  // Funcion para ocultar el menu
  function hide_menu(){
    // Si se ve en un movil, mostramos todos los posits
    if ( $(window).width() <= 1000 ) {
      for (var i = 1; i < 4; i++) {
        $("#posit"+i+"").css("display", "block");
      }
      $(".text_one_page").css("display", "block");
    }
    // Animaciones para el menu
    $("#sections_menu").css("animation","menu_animation_exit 0.5s linear both");  // Le añado una animacion para la salida del menu
    $("#hide_menu").css("animation","exit_hide_menu 0.5s linear both");  // Le añado una animacion de salida al boton hide menu
    setTimeout('$("#hide_menu").css("display", "none")', 600); // Hago que desaparezca el div de la pantalla despues de 600 milisegundos
    setTimeout('$("#see_menu").css("display", "block");', 450);  // Para volver a poner el div de mostrar menu en 450 milisegundos
  }

  // Cuando se clica a la div de hide_menu se ejecuta la funcion hide_menu
  $("#hide_menu").click(function() {
      hide_menu();
  });

  // Funcion para cambiar el color de fondo de la imagen de see_menu
  $(window).scroll(function(){
    // Si ha pasado la primera pagina se pone el fondo transparente
    if ( $(window).scrollTop() >= "1024" ){
      $("#see_menu").css("background-color", "transparent");
    }
    // Si esta en la primera pagina se pone el fondo blanco
    else {
      $("#see_menu").css("background-color", "white");
    }
  });


  // Si le damos click a inicio, ejecutamos una funcion para que vaya a la parte de inicio
  $("#section_inicio").click(function(){
    $('html, body').animate({scrollTop: ($('#Inicio').offset().top)},500);
    // Si la pantalla es del tamaño del movil se llama a la funcion para ocultar el menu
    if ( $(window).width() <= 1000 ) {
      hide_menu();
    }
  });

    // Si le damos click a caracteristicas, ejecutamos una funcion para que vaya a la parte de caracteristicas
  $("#section_caracteristicas").click(function(){
    $('html, body').animate({scrollTop: ($('#caracteristicas').offset().top)},500);
    // Si la pantalla es del tamaño del movil se llama a la funcion para ocultar el menu
    if ( $(window).width() <= 1000  ) {
      hide_menu();
    }
  });

    // Si le damos click a materiales, ejecutamos la funcion para que vaya a la parte de materiales
    $("#section_materiales").click(function(){
      $('html, body').animate({scrollTop: ($('#materiales').offset().top)},500);
      // Si la pantalla es del tamaño del movil se llama a la funcion para ocultar el menu
      if ( $(window).width() <= 1000  ) {
        hide_menu()
      }
    });

  // Si le damos click a galeria, funcion para que vaya a la parte de galeria
  $("#section_galeria").click(function(){
    $('html, body').animate({scrollTop: ($('#galeria').offset().top)},500);
    // Si la pantalla es del tamaño del movil se llama a la funcion para ocultar el menu
    if ( $(window).width() <= 1000  ) {
      hide_menu()
    }
  });

  // Si le damos click a contacto, funcion para que vaya a la parte de contacto en la pagina
  $("#section_contacto").click(function(){
    $('html, body').animate({scrollTop: ($('#contacto').offset().top)},500);
    // Si la pantalla es del tamaño del movil se llama a la funcion para ocultar el menu
    if ( $(window).width() <= 1000  ) {
      hide_menu()
    }
  });




  // --------------
  //    Carousel
  //    Imagenes
  // --------------
  // Cojemos las imagenes y le asignamos 0 a index que es la primera imagen
  var cimages = $(".img_carousel");
  var index = 0;

  // Cuando se hacen click a los botones se llaman a las funciones
  $('#button1').click(next);
  $('#button2').click(previous);

  // Funcion para pasar a la siguiente imagen
  function next() {
    // Oculta la imagen actual
    $(cimages[index]).css("display", "none");
    // $(cimages[index]).css("animation", "next_image 0.5s 1 forwards");

    // Suma uno al index para mostrar la siguiente foto
    index = index + 1;
    // Si el índice es igual a la ultima foto
     if (index == cimages.length) {
       // Actualizamos el índice a 0 para apunta a la primera imagen
       index = 0;
     }
     // Mostramos la siguiente imagen
     $(cimages[index]).css('display', 'block');
     // $(cimages[index]).css("animation", "see_next_image 0.5s 1 forwards");
  }

  // Funcion para mostrar a la imagen anterior
  function previous() {
    // Oculta la imagen actual
    $(cimages[index]).css("display" , "none");
    // Resta uno al index para mostrar la foto anterior
    index = index - 1;
    // Si el índice es menor que 0 hemos llegado a la primera imagen, la siguiente en mostrar es la última
    if (index < 0) {
      // Actualizamos el índice con el valor de la última imagen
      index = cimages.length - 1;
    }
    // Mostramos la imagen anterior
    $(cimages [index]).css("display", "block");
  }


  // --------------
  //    Carousel
  //    Videos
  // --------------
  // Cojemos las imagenes y le asignamos 0 a index que es la primera imagen
  var videos = $(".videos_carousel");
  var index_videos = 0;

  // Cuando se hacen click a los botones se llaman a las funciones
  $('#button3').click(previous_video);
  $('#button4').click(next_video);

  // Funcion para pasar al siguiente video
  function next_video() {
    // Oculta el video actual
    $(videos[index_videos]).css("display", "none");
    // Suma uno al index para mostrar el siguiente video
    index_videos = index_videos + 1;
    // Si el índice es igual al ultimo video
     if (index_videos == videos.length) {
       // Actualizamos el índice a 0 para apunta al primer video
       index_videos = 0;
     }
     // Mostramos el siguiente video
     $(videos[index_videos]).css("display", "block");
  }

  // Funcion para mostrar el siguiente video
  function previous_video() {
    // Oculta el video actual
    $(videos[index_videos]).css("display", "none");
    // Resta uno al index para mostrar el video anterior
    index_videos = index_videos - 1;
    // Si el índice es menor que 0 hemos llegado al primer video, el siguiente en mostrar es ultimo
    if (index_videos < 0) {
      // Actualizamos el índice con el valor del ultimo video
      index_videos = videos.length - 1;
    }
    // Mostramos el video anterior
    $(videos[index_videos]).css("display", "block");
  }


  // ********************************
  // Enviar el formulario de contacto
  // ********************************
// Cuando se da click en el boton de enviar formulario
   $("#boton_enviar").click(function(){
	   // Elimino la div de mensaje de error y se le vuelve a dar al boton de enviar formulario
	   $("#div_mensaje_error").remove();
	   $("#div_mensaje_enviado").remove();

	   // Si no ha puesto el nombre ni el correo
	   if ($("#input_name").val() == "" && $("#mail").val() == "" ){
		 $("#inside_form_contact").append("<div id='div_mensaje_error'>El nombre y el e-mail son obligatorios.</div>");
	   }

	   // Si no ha puesto el nombre en el formulario
	   else if ($("#input_name").val() == "" ){
		$("#inside_form_contact").append("<div id='div_mensaje_error'>El nombre es obligatorio.</div>");
	   }

	   // Si no ha puesto correo en el formulario
	   else if ($("#mail").val() == "") {
		 $("#inside_form_contact").append("<div id='div_mensaje_error'>El e-mail es obligatorio.</div>");
	   }

	   // Si todo est correcto, se llama al php pasando los atributos con sertialize
	   else {
		$.post( "enviar_correo.php", $( "#formulario" ).serialize(), function( data ) {
		    $("#inside_form_contact").append(data); // Añado el la div de inside_form_contact los datos que hay en el echo de enviar_correo.php
		});
		return false;
	   }
   });



});
